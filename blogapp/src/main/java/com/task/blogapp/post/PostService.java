package com.task.blogapp.post;

import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;


public interface PostService {

    Optional<Post> findById(long id);

    List<Post> findAllByOrderByLastModifiedDesc();

    void addAndUpdatePost(Post post);

    void deletePost(Post post);

}
