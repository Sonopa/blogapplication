package com.task.blogapp.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@Controller
public class PostController {
    @Autowired
    PostService postService;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("posts", postService.findAllByOrderByLastModifiedDesc());

        return "index";
    }

    @GetMapping("/post")
    public String newPost(Model model) {
        Post post = new Post();
        model.addAttribute("post", post);
        return "edit-post";
    }

    @GetMapping("/post/{id}")
    public String post(@PathVariable("id") long id, Model model) {
        Optional<Post> post = postService.findById(id);
        model.addAttribute("post", post.get());
        return "post";
    }

    @RequestMapping("/post/edit/{id}")
    public String editPost(@PathVariable("id") long id, Model model) {
        Optional<Post> post = postService.findById(id);
        model.addAttribute("post", post.get());
        return "edit-post";
    }

    @PostMapping("/post")
    public String updatePost(Post post) {

        Post postToSave = Optional.ofNullable(post.getId())
                .flatMap(id -> postService.findById(id))
                .map(postToEdit -> {
                    postToEdit.setLastModified(LocalDateTime.now());
                    postToEdit.setBody(post.getBody());
                    postToEdit.setTitle(post.getTitle());

                    return postToEdit;
                })
                .orElseGet(() -> {
                    LocalDateTime now = LocalDateTime.now();
                    post.setLastModified(now);

                    return post;
                });

        postService.addAndUpdatePost(postToSave);

        return "redirect:/";
    }

    @GetMapping("/post/{id}/delete")
    public String deletePost(@PathVariable("id") long id) {
        Post post = postService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid post Id:" + id));

        postService.deletePost(post);
        return "redirect:/";
    }
}
