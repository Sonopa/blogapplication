package com.task.blogapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.util.SocketUtils;

import java.io.IOException;
import java.net.Socket;

@SpringBootApplication
public class BlogappApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogappApplication.class, args);
	}

	@Bean
	public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> containerCustomizer() {
		return factory -> {
			int port = 8080;
			if (!isPortAvailable(port)) {
				port = SocketUtils.findAvailableTcpPort(8080, 9090);
				System.getProperties().put("server.port", port);
			}

			factory.setPort(port);
			factory.setContextPath("");
		};
	}

	private static boolean isPortAvailable(int port) {
		try (Socket ignored = new Socket("localhost", port)) {
			return false;
		} catch (IOException ignored) {
			return true;
		}
	}

}
